<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>KCMS TABLE CFG</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel='stylesheet' type='text/css' href='css/tabla.css'>
    </head>
    <body>
        <table style="width:100%">
            <tr>
                <td style="min-width:100px;text-align:right">TABLA ID:</td>
                <td style="min-width:200px">
                    <input type="text" id="id" style="width:100%;box-sizing: border-box" readonly>
                </td>
                <td style="min-width:100px;text-align:right">TABLA DESC:</td>
                <td style="width:100%">
                    <input type="text" id="desc" style="width:100%;box-sizing: border-box">
                </td>
            </tr>
        </table>
        <hr>
        <span id="perfiles"><input type="checkbox">noticias</span>
        <span style="float:right;margin-top:-2px;margin-bottom: 7px">
            <input onchange="setTablaEditable(this.checked)" id="editable" type="checkbox" style='margin-right:12px'>Agregar/Quitar/Ordenar
            <a href="javascript: colAdd()" title="Nueva columna"><img src="icons/add2.png" style="cursor:pointer;margin-top:-4px;margin-bottom:-7px" alt="Nueva columna"></a>
        </span>
        <hr style="clear:both">
        <ul id="items"></ul>
        <script src="js/sortable.js"></script>
        <script src="js/utils.js"></script>
        <script type="text/javascript">
            addLogErrLoad();
            onEscape(parent.hideModal);
            var tabla = location.search.substring(1);
            var data;
            $("id").value = tabla;
            function updatePerfiles() {
                log(data);
            }

            var dontUpdate = false;
            function updateView() {
                var cols = $("items");
                var html = [];
                var tipos = ['texto', 'numero', 'fecha', 'logico', 'opcion', 'html', 'archivo', 'id', 'subtabla'];
                for (var i = 0; i < data.columns.length; i++) {
                    var col = data.columns[i];
                    html.push('<li><table style="width:100%"><tr><td style="width:460px"><span class="sort"></span><input id="' + i + '.edit" type="checkbox"' + (col.edit ? " checked" : "") + '> Editable' +
                            '<input type="text" id="' + i + '.nombre" placeholder="Nombre" value="' + col.title + '"><input type="text" id="' + i + '.ancho" value="' + (col.width > 0 ? col.width : "") +
                            '" placeholder="Ancho" style="width:40px" onkeypress="return event.charCode >= 48 && event.charCode <= 57">' +
                            '<select id="' + i + '.tipo" placeholder="Tipo" value="' + col.type + '">');
                    for (var j = 0; j < tipos.length; j++)
                        html.push('<option>' + tipos[j] + "</option>");
                    html.push('</select>' +
                            '</td><td><input type="text" id="' + i + '.reglas" value="' + col.rules + '" placeholder="Reglas" style="width:100%;box-sizing: border-box;position:relative;bottom:-3px">' +
                            '</td><td style="width:24px"><a title="Elimina esta columna" href="javascript: colDel(' + i + ')">' +
                            '<img src="icons/del2.png" style="cursor:pointer;margin-top:-4px;margin-bottom:-7px" alt="Elimina esta columna"></a></td></tr></table></li></ul>');
                }
                cols.innerHTML = html.join("");
                dontUpdate = true;
                for (var idx = 0; idx < data.columns.length; idx++) {
                    events(idx);
                }
                dontUpdate = false;
            }

            function events(i) {
                var inp = $(i + '.edit');
                inp.onclick = function () {
                    if (!dontUpdate && inp.checked !== data.columns[i].edit)
                        ajax("colEdit", [tabla, i, inp.checked], function () {
                            data.columns[i].edit = inp.checked;
                            log("Columna editable modificado...");
                        });
                };
                var inp1 = $(i + '.nombre');
                onInputChange(inp1, function () {
                    if (!dontUpdate && inp1.value !== data.columns[i].titulo)
                        ajax("colName", [tabla, i, inp1.value], function () {
                            data.columns[i].titulo = inp1.value;
                            log("Columna titulo modificado...");
                        });
                });
                var inp2 = $(i + '.ancho');
                onInputChange(inp2, function () {
                    if (!dontUpdate && inp2.value !== data.columns[i].ancho)
                        var ancho = inp2.value === '' ? '0' : inp2.value;
                    ajax("colAncho", [tabla, i, ancho], function () {
                        data.columns[i].ancho = parseInt(ancho);
                        log("Columna ancho modificado...");
                    });
                });
                var inp3 = $(i + '.tipo');
                inp3.value = data.columns[i].type;
                inp3.onchange = function () {
                    if (!dontUpdate && inp3.value !== data.columns[i].type)
                        ajax("colTipo", [tabla, i, inp3.value], function () {
                            data.columns[i].type = inp3.value;
                            log("Columna tipo modificado...");
                        });
                };
                var inp4 = $(i + '.reglas');
                onInputChange(inp4, function () {
                    if (!dontUpdate && inp4.value !== data.columns[i].reglas)
                        ajax("colReglas", [tabla, i, inp4.value], function () {
                            data.columns[i].reglas = inp4.value;
                            log("Columna reglas modificado...");
                        });
                });

            }

            function colDel(idx) {
                ajax("colDel", [tabla, idx], function (resp) {
                    data = JSON.parse(resp);
                    updateView();
                    log("Columna eliminada...");
                });
            }

            function updateTabla() {
                loading(true);
                ajax("getTable", [tabla], function (resp) {
                    data = JSON.parse(resp);
                    ajax("getPerfiles", null, function (resp) {
                        var perf = JSON.parse(resp);
                        var html = [];
                        if (perf.length > 1)
                            html.push("PERFILES: ");
                        for (var i = 0; i < perf.length; i++) {
                            if (perf[i] !== 'admin')
                                html.push("<input type='checkbox'" + (data.profiles.indexOf(perf[i]) > -1 ? " checked" : "")
                                        + " onchange='setPerfil(\"" + tabla + "\",\"" + perf[i] + "\",this)'>" + perf[i] + " ");
                        }
                        $("perfiles").innerHTML = html.join("");
                        if (perf) {
                            updateView();
                        }
                        loading(false);
                    }, function (resp) {
                        err(resp);
                        loading(false);
                    }, false);
                    $("desc").value = data.desc;
                    $("editable").checked = data.edit;
                    onInputChange($("desc"), function (e) {
                        if (data.desc !== e.value)
                            ajax("setTablaDesc", [tabla, e.value], function () {
                                data.desc = e.value;
                                log("Descripción modificada...");
                            });
                    });
                });
            }
            updateTabla();
            function colAdd() {
                ajax("colAdd", [tabla], function (resp) {
                    log("Nueva columna agregada...");
                    data = JSON.parse(resp);
                    updateView();
                });
            }

            function setTablaEditable(check) {
                ajax("setTablaEditable", [tabla, check], function () {
                    log("Tabla es " + (check ? " " : "no ") + "modificable...");
                });
            }
            
            function del() {
                ajax("tablaDel", [tabla], function () {
                    parent.hideModal();
                    setTimeout(function () {
                        parent.dirTree();
                    }, 300);
                });
            }

            function setPerfil(tab, per, check) {
                var val = check.checked;
                ajax("setTablaPerfil", [tab, per, val], function () {
                    if (val) {
                        data.profiles.push(per);
                        log("Perfil " + per + " agregado");
                    } else {
                        var idx = data.profiles.indexOf(per);
                        data.profiles.splice(idx, 1);
                        log("Perfil " + per + " elimnado");
                    }
                });
            }

            var el = $('items');
            var from;
            var sortable = new Sortable(el, {
                animation: 250,
                handle: ".sort",
                onSort: function (evt) {
                    ajax("colSort", [tabla, from, evt.newIndex], function () {
                        var c = data.columns[from];
                        data.columns.splice(from,1);
                        data.columns.splice(evt.newIndex,0,c);
                        updateView();
                        log("Columna orden modificado...");
                    });
                },
                onStart: function (evt) {
                    from = Array.prototype.slice.call(evt.item.parentNode.children).indexOf(evt.item);
                }
            });
        </script>
    </body>

</html>
