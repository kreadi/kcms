<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String contentId = request.getParameter("contentId");
    String tableId = request.getParameter("tableId");
    int x = Integer.parseInt(request.getParameter("x"));
    int y = Integer.parseInt(request.getParameter("y"));
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                margin:0;padding: 0;
            }
            textarea{
                position: fixed;
                bottom:0;
                width:100%;
                height: 100%;
            }
        </style>
    </head>
    <body>
        <textarea id="html" name="html"></textarea>
        <script src="ckeditor/ckeditor.js"></script>
        <script src="js/utils.js"></script>
        <script>
            var editor = CKEDITOR.replace('html', {
                extraPlugins: 'magicline,showblocks',
                startupOutlineBlocks: true
            });
            
            var oh = -1;
            function resize() {
                var h = window.innerHeight;
                if (oh !== h) {
                    oh = h;
                    if (editor)
                        editor.resize('100%', h + '');
                }
            }

            CKEDITOR.plugins.registered['save'] = {
                init: function (editor)
                {
                    editor.addCommand('save',
                            {
                                modes: {wysiwyg: 1, source: 1},
                                exec: function () {

                                    ajax("setHtml", ["<%=tableId%>", <%=y%>, <%=x%>, editor.getData()], function (resp) {
                                        parent.cellData[<%=y%>][<%=x%>]=resp;
                                        parent.log("Html guardado...");
                                    });

                                }
                            }
                    );
                    editor.ui.addButton('Save', {label: 'Guardar', command: 'save'});
                    setInterval(resize, 250);
                    editor.focus();
                }
            };

            <%if (!"undefined".equals(contentId)){%>
            var xhr = new XMLHttpRequest();
            xhr.open('get', '/admin/serve?<%=contentId%>');

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        editor.setData(xhr.responseText);
                    } else {
                        parent.err('Error: ' + xhr.status); // An error occurred during the request
                    }
                }
            };
            xhr.send(null);
            <%}%>
            

        </script>
    </body>
</html>
