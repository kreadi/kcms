<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String contentId = request.getParameter("contentId");
    String contentName = request.getParameter("contentName");
    String tableId = request.getParameter("tableId");
    int x = Integer.parseInt(request.getParameter("x"));
    int y = Integer.parseInt(request.getParameter("y"));
%>
<!DOCTYPE html>
<html manifest="editCode.appcache">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>KCMS Code Editor</title>
        <link rel='stylesheet' type='text/css' href='css/style.css'>
        <link rel='stylesheet' type='text/css' href='codemirror/codemirror.css'>
        <link rel='stylesheet' type='text/css' href='codemirror/addon/hint/show-hint.css'>
        <link rel="stylesheet" type='text/css' href="codemirror/addon/fold/foldgutter.css" />
        <link rel="stylesheet" type='text/css' href="codemirror/addon/search/matchesonscrollbar.css" />
        <link rel="stylesheet" type='text/css' href="codemirror/addon/dialog/dialog.css" />
        <style>
            body{
                background: gray;
                margin:0;
                padding:0;
            }
            .CodeMirror{
                display: block;
                position:absolute;
                height:auto;
                bottom:0;
                top:0;
                left:0;
                right:0;
                margin-top:32px;
            }
            a img{
                margin-top:4px;


                margin-right: 8px;
                background:rgba(255,255,255,.5);
            }
        </style>
    </head>
    <body>
        &nbsp;<a href="javascript:save()" title="Guardar CTRL+S"><img src="icons/save.png" alt="save"></a>
        <div style="display: inline-block;float:right">
            <a href="javascript:search()" title="Buscar CTRL+F"><img src="icons/search.png" alt="search"></a>
            <a href="javascript:next()" title="Buscar Siguiente CTRL+G"><img src="icons/next.png" alt="next"></a>
            <a href="javascript:prev()" title="Buscar Anterior CTRL+SHIFT+G"><img src="icons/prev.png" alt="prev"></a>
            <a href="javascript:rename()" title="Remplazar CTRL+SHIFT+R"><img src="icons/rename.png" alt="save"></a>
        </div>
        <textarea id="code" name="code"></textarea>

        <script src="codemirror/codemirror.js"></script>

        <script src="codemirror/addon/hint/show-hint.js"></script>
        <script src="codemirror/addon/hint/css-hint.js"></script>
        <script src="codemirror/addon/hint/html-hint.js"></script>
        <script src="codemirror/addon/hint/xml-hint.js"></script>
        <script src="codemirror/addon/hint/javascript-hint.js"></script>

        <script src="codemirror/addon/fold/foldcode.js"></script>
        <script src="codemirror/addon/fold/foldgutter.js"></script>
        <script src="codemirror/addon/fold/brace-fold.js"></script>
        <script src="codemirror/addon/fold/xml-fold.js"></script>
        <script src="codemirror/addon/fold/markdown-fold.js"></script>
        <script src="codemirror/addon/fold/comment-fold.js"></script>

        <script src="codemirror/addon/search/searchcursor.js"></script>
        <script src="codemirror/addon/dialog/dialog.js"></script>
        <script src="codemirror/addon/scroll/annotatescrollbar.js"></script>
        <script src="codemirror/addon/search/search.js"></script>
        <script src="codemirror/addon/search/matchesonscrollbar.js"></script>

        <script src="codemirror/addon/edit/matchbrackets.js"></script>
        <script src="codemirror/addon/comment/continuecomment.js"></script>
        <script src="codemirror/addon/comment/comment.js"></script>

        <script src="codemirror/mode/javascript/javascript.js"></script>
        <script src="codemirror/mode/xml/xml.js"></script>
        <script src="codemirror/mode/css/css.js"></script>
        <script src="codemirror/mode/htmlmixed/htmlmixed.js"></script>
        <script src="codemirror/mode/htmlembedded/htmlembedded.js"></script>

        <script src="js/utils.js"></script>
        <script>
            addLogErrLoad();

            function save() {
                loading(true);
                ajax("setCode", ["<%=tableId%>", <%=y%>, <%=x%>, editor.getValue()], function (resp) {
                    loading(false);
                    parent.cellData[<%=y%>][<%=x%>] = JSON.parse(resp);
                    log("Código guardado...");
                });
            }

            function search() {
                CodeMirror.commands.find(editor);
                return false;
            }

            function next() {
                CodeMirror.commands.findNext(editor);
                return false;
            }

            function prev() {
                CodeMirror.commands.findPrev(editor);
                return false;
            }

            function rename() {
                CodeMirror.commands.replaceAll(editor);
                return false;
            }


            var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                lineNumbers: true,
                lineWrapping: true,
                extraKeys: {
                    "Ctrl-Space": "autocomplete",
                    "Ctrl-Q": function (cm) {
                        cm.foldCode(cm.getCursor());
                    },
                    "Ctrl-S": function (cm) {
                        save();
                    },
                    "Esc": function (cm) {
                        parent.hideModal();
                    }
                },
                autofocus: true,
                foldGutter: true,
                matchBrackets: true,
                autoCloseBrackets: true,
                gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
            });

            <%if (!"undefined".equals(contentId)) {%>
            var xhr = new XMLHttpRequest();
            xhr.open('get', '/admin/serve?<%=contentId%>');

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        editor.setValue(xhr.responseText);
                        var name = "<%=contentName%>";
                        var idx = name.lastIndexOf(".");
                        var ext = idx === -1 ? "" : name.substring(idx + 1).toUpperCase();
                        if (ext === "" || ext === "HTM" || ext === "HTML") {
                            editor.setOption("mode", {name: "application/x-ejs"});
                        } else if (ext === "CSS") {
                            editor.setOption("mode", {name: "css"});
                        } else if (ext === "JS") {
                            editor.setOption("mode", {name: "javascript", globalVars: true});
                        } else if (ext === "JSON") {
                            editor.setOption("mode", {name: "application/ld+json"});
                        }
                    } else {
                        err('Error: ' + xhr.status);
                    }
                }
                parent.loading(false);
            };
            xhr.send(null);
            <%} else {%>
            editor.setValue("");
            var name = "<%=contentName%>";
            var idx = name.lastIndexOf(".");
            var ext = idx === -1 ? "" : name.substring(idx + 1).toUpperCase();
            if (ext === "" || ext === "HTM" || ext === "HTML") {
                editor.setOption("mode", {name: "application/x-ejs"});
            } else if (ext === "CSS") {
                editor.setOption("mode", {name: "css"});
            } else if (ext === "JS") {
                editor.setOption("mode", {name: "javascript", globalVars: true});
            } else if (ext === "JSON") {
                editor.setOption("mode", {name: "application/ld+json"});
            }
            parent.loading(false);
            <%}%>
        </script>
    </body>
</html>
