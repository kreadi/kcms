<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@page import="com.google.appengine.api.users.UserService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    UserService userService = UserServiceFactory.getUserService();
    String url = request.getRequestURI();
    String logout = null;
    String user = null;

    response.setContentType("text/html");
    if (request.getUserPrincipal() == null) {
        response.sendRedirect(userService.createLoginURL(url));
    } else {
        user = userService.getCurrentUser().getEmail();
        logout = userService.createLogoutURL(url);
%>
<!DOCTYPE html>
<html manifest="editCode.appcache">
    <head>
        <title>KCMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" type="image/png" href="icons/k.png"/> 
        <link rel='stylesheet' type='text/css' href='css/style.css'>

    </head>

    <body>

        <iframe id="frame" style="box-sizing: border-box;border: 0;outline: none;background:white;position: fixed;top:28px;width:100%;height:100%;bottom:0px;">
        </iframe>

        <div id="top">
            KCMS &nbsp;
            <a href="javascript:incognito()"><img src="icons/play.png" alt="Ver Web"></a> &nbsp;
            <select id='tree' onchange="javascript:updateTabla()"></select> &nbsp;


            <div style="float:right;padding-right: 12px">
                <span> <%=user%> </span>
                <a href="javascript:addTabla()" title="Nueva tabla"><img src="icons/tabla.add.png" alt="Nueva Tabla"></a>
                <a href="javascript:delTabla()" id="et" title="Eliminar tabla"><img src="icons/tabla.del.png" alt="Eliminar Tabla"></a>
                <a href="javascript:renTabla()" id="rt" title="Renombrar tabla"><img src="icons/tabla.ren.png" alt="Renombrar Tabla"></a>
                <a href="javascript:showModal('tabla.jsp?'+$('tree').value)" id="ct" title="Configuración de la tabla"><img src="icons/tabla.png" alt="Tabla Configuración"></a>
                <a href="javascript:showModal('config.jsp')" title="Configuración"><img src="icons/config.png" alt="Configuración"></a>
                <a href="javascript:showModal('ayuda.jsp')" title="Ayuda"><img src="icons/help.png" alt="Ayuda"></a>
                <a href="<%=logout%>" title="Salir"><img src="icons/logout.png" alt="Salir"></a>
            </div>

        </div>
        <script src="js/sortable.js"></script>
        <script src="js/utils.js"></script>
        <script>
                var def, data, tablas, path, disableCheckTablaChange = false;

                function incognito() {
                    window.open('/', 'test page', 'toolbar=0,titlebar=0,menubar=0,location=0,status=0,scrollbars=1,width=1200,height=600');
                }

                function updateHeight() {
                    $('frame').style.height = (window.innerHeight - 28) + "px";
                }

                window.onresize = updateHeight;
                updateHeight();

                function updateTabla() {
                    if (!disableCheckTablaChange) {
                        var tree = $("tree");
                        if (tree.value) {
                            path = tree.value;
                            updateData(path);
                            log("load path " + path);
                        }
                    }
                }

                function updateData(pth) {
                    $("frame").src = "data.jsp?" + pth;
                }

                function updateTablas(pth) {
                    var tree = $("tree");
                    if (tablas.data.length > 0) {
                        $("et").style.display = "inline-block";
                        $("rt").style.display = "inline-block";
                        $("ct").style.display = "inline-block";
                    } else {
                        $("et").style.display = "none";
                        $("rt").style.display = "none";
                        $("ct").style.display = "none";
                    }
                    tree.innerHTML = "";
                    for (var i = 0; i < tablas.data.length; i++) {
                        var tabla = tablas.data[i];
                        var node = document.createElement("option");
                        node.innerHTML = tabla;
                        node.value = tabla;
                        tree.appendChild(node);
                    }
                    if (pth) {
                        tree.value = pth;
                        updateData(pth);
                        path = pth;
                    }
                }

                function dirTree() {
                    loading(true);
                    ajax("tablasGet", null, function (resp) {
                        tablas = {data: JSON.parse(resp)};
                        if (tablas.data.length > 0)
                            path = tablas.data[0];
                        log(resp);
                        updateTablas(path);
                        loading(false);
                    }, function (resp) {
                        loading(false);
                        err(resp);
                    });
                }
                addLogErrLoad();
                dirTree();

                function addTabla() {
                    var pth = askPath();
                    if (pth) {
                        ajax("tablasAdd", [pth], function (resp) {
                            disableCheckTablaChange = true;
                            tablas = {data: JSON.parse(resp)};
                            $("tree").value = pth;
                            updateTablas(pth);
                            path = pth;
                            disableCheckTablaChange = false;
                            log("tabla " + pth + " creada.");
                        });
                    }
                }

                function delTabla() {
                    path = $("tree").value;
                    if (confirm("Desea Eliminar la tabla " + path))
                        ajax("tablaDel", [path], function () {
                            var idx = tablas.data.indexOf(path);
                            if (idx > -1) {
                                tablas.data.splice(idx, 1);
                                if (tablas.data.length - 1 < idx && idx > 0)
                                    idx = tablas.data.length - 1;
                                if (tablas.data.length > 0)
                                    updateTablas(tablas.data[idx]);
                                else {
                                    path = undefined;
                                    updateTablas();
                                }
                                log("Tabla eliminada...");
                            } else
                                err("NOSE VER");
                        });
                }

                function askPath(oldPath) {
                    var old = oldPath;
                    if (oldPath===undefined) {
                        oldPath = "";
                    }
                    var newPath = prompt("Ruta de la tabla", oldPath);
                    if (newPath===null)
                        return null;
                    newPath = newPath.trim();
                    if (!newPath.startsWith("/"))
                        newPath = "/" + newPath;
                    if (newPath.indexOf("//") > -1 || newPath.indexOf(" ") > -1) {
                        err("Formato no valido (ejemplos: fonts /css/img");
                        return null;
                    }
                    if (newPath.length > 1 && newPath.substring(newPath.length - 1) === "/")
                        newPath = newPath.substring(0, newPath.length - 1);
                    if (newPath !== oldPath) {
                        if (tablas.data.indexOf(newPath) > -1) {
                            if (old)
                                return null;
                            err("Ya existe la tabla " + newPath + ", elija otro.");
                        } else if (!newPath.match(/[a-zA-Z0-9\/\-\.\_]+/)) {
                            err("Formato no valido (ejemplos: fonts /css/img");
                        } else {
                            return newPath;
                        }
                    } else
                        return null;
                }

                function renTabla() {
                    path = $("tree").value;
                    var val = askPath(path);
                    if (val) {
                        ajax("tablasRen", [path, val], function (resp) {
                            disableCheckTablaChange = true;
                            path = val;
                            tablas = {data: JSON.parse(resp)};
                            updateTablas(val);
                            log("Tabla renombrada...");
                            disableCheckTablaChange = false;
                        });
                    }
                }

                document.onkeydown = function (evt) {
                    evt = evt || window.event;
                    if (evt.keyCode === 27 && isModal)
                        hideModal();
                };
                window.onclick = function (evt) {
                    if (evt.target.tagName === "HTML")
                        console.log(1);
                };
        </script>
    </body>

</html>
<%}%>