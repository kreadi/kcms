<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>KCMS</title>
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel='stylesheet' type='text/css' href='css/style.css'>
    </head>
    <body>
        <div style="padding-left:2px">
            <div id="itemHeader" style="margin-top:-1px">
            </div>
            <ul id="items">
            </ul>
        </div>
        <div id="top">
            Tabla: <script>document.write(location.search.substring(1));</script>
            <img style="float:right;margin-right:12px;top: -3px;position: relative" src="icons/del.png" id="del" alt="Eliminar Items" onclick='javascript: rowDel()'>
            <img style="float:right;margin-right:8px;top: -3px;position: relative" src="icons/add.png" id="add" alt="Agregar Item" onclick='javascript: rowAdd()'>
        </div>
        <script src="js/sortable.js"></script>
        <script src="ckeditor/ckeditor.js"></script>
        <script src="js/utils.js"></script> 
        <script>

                document.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
                    alert(errorMsg);
                    // Just let default handler run.
                    return true;
                };

                var def, cellData, path = location.search.substring(1);

                function updateData(pth) {
                    log("load path " + path);
                    loading(true);
                    ajax("getTable", [pth], function (resp) {
                        def = JSON.parse(resp);
                        cellData = def.data;
                        def.data = undefined;
                        renderData();
                        loading(false);
                    });
                }

                function renderData() {
                    var itemHeader = $("itemHeader");
                    var items = $("items");
                    var html = [];
                    html.push('<style>');
                    for (var i = 0; i < def.columns.length; i++) {
                        var col = def.columns[i];
                        html.push('.c' + i + '{' + (col.width > 0 ? ('min-width:' + col.width + 'px') : 'width:100%') + '}');
                    }
                    if (def.columns.length > 0) {
                        html.push('.cx{min-width: 24px} .ce{min-width: 24px;padding:6px 6px 0 6px !important;} .ce input{cursor: default;}');
                        $("add").style.display = "inline-block";
                        $("del").style.display = "inline-block";
                    } else {
                        $("add").style.display = "none";
                        $("del").style.display = "none";
                    }
                    html.push('</style>');
                    html.push('<div style="display: table-row">');
                    if (def.columns.length > 0)
                        html.push('<div class="cell cx"></div>');
                    for (var i = 0; i < def.columns.length; i++) {
                        var col = def.columns[i];
                        html.push('<div class="cell c' + i + '">' + (col.title.trim().length > 0 ? col.title : "&nbsp;") + '</div>');
                    }
                    if (def.columns.length > 0)
                        html.push('<div class="cell ce"><input type="checkbox" onchange="checkall(this.checked)"></div>');
                    html.push('</div>');
                    itemHeader.innerHTML = html.join("");
                    html = [];

                    for (var i = 0; i < cellData.length; i++) {
                        html.push('<li><div class="cell cx"><span class="sort"></span></div>');
                        for (var j = 0; j < cellData[i].length; j++) {
                            var tipo = def.columns[j].type;
                            html.push('<div id="c.' + j + '.' + i + '" class="cell c' + j + '">');
                            if (tipo === 'archivo') {
                                html.push('<span class="upload" onclick="fileSelector.value=\'\';fileSelector.click();return false;" ></span>' +
                                        '<span class="download" onclick="down(' + j + ',' + i + ')"></span>' +
                                        '<span class="edit"></span>');
                                if (cellData[i][j])
                                    html.push(cellData[i][j].name);
                                else
                                    html.push("&nbsp;");
                            } else {
                                if (cellData[i][j])
                                    html.push(cellData[i][j]);
                                else
                                    html.push("&nbsp;");
                            }
                            html.push("</div>");
                        }
                        html.push('<div class="cell ce"><input class="check" type="checkbox" style="cursor:pointer"></div></li>');
                    }
                    if (html.length > 0)
                        items.innerHTML = html.join("");
                    else
                        items.innerHTML = "";
                    var check = _("check");
                    for (var i = 0; i < check.length; i++)
                        check[i].onchange = function () {
                            rerender(this);
                        };
                    var cells = _("cell");
                    for (var i = 0; i < cells.length; i++) {
                        if (cells[i].id.indexOf("c.") === 0)
                            cells[i].onclick = function (ev) {
                                var tar = ev.target.className;
                                if (tar !== 'download') {
                                    var sp = this.id.split(".");
                                    var x = parseInt(sp[1]);
                                    var y = parseInt(sp[2]);
                                    var tipo = def.columns[x].type;
                                    edit(this, tipo, x, y, tar.indexOf("cell ") === 0 ? "cell" : tar);
                                }
                            };
                    }
                }


                function down(col, row) {
                    var val = cellData[row][col];
                    var id = val.id;
                    if (id) {
                        var enc = encodeURI(id);
                        cellEdit = undefined;
                        window.open("/admin/serve?" + enc);
                    }
                }

                var fileSelector = document.createElement('input');
                fileSelector.setAttribute('type', 'file');
                fileSelector.setAttribute('name', 'myFile');
                fileSelector.onchange = function () {
                    if (fileSelector.value !== '')
                        ajax("uploadUrl", [], function (resp) {
                            var xhr = new XMLHttpRequest();
                            var total;
                            xhr.upload.addEventListener('progress', function (ev) {
                                parent.log(Math.round(100 * (ev.loaded / ev.total)) + '%');
                                total = ev.total;
                            }, false);
                            xhr.onload = function () {
                                if (xhr.status === 200) {
                                    var idx = fileSelector.value.lastIndexOf("\\") + 1;
                                    var name = fileSelector.value.substring(idx);
                                    var id = xhr.response;
                                    ajax("setArchivo", [path, name, id, cellEdit.x, cellEdit.y, total], function (respId) {
                                        id = respId;
                                        var val = {id: id, size: total, name: name};
                                        var html = cellEdit.cell.innerHTML;
                                        html = html.substring(0, html.lastIndexOf(">") + 1) + name;
                                        cellEdit.cell.innerHTML = html;
                                        cellData[cellEdit.y][cellEdit.x] = val;
                                        cellEdit = undefined;
                                    });
                                }
                                parent.loading(false);
                            };
                            xhr.open('POST', resp, true);
                            var files = fileSelector.files;
                            var data = new FormData();
                            for (var i = 0; i < files.length; i++)
                                data.append('myFile', files[i]);
                            try {
                                parent.loading(true);
                                xhr.send(data);
                            } catch (e) {

                            }
                        });
                };

                var cellEdit;
                function edit(cell, edit, x, y, text) {
                    if (!cellEdit) {
                        cellEdit = {cell: cell, edit: edit, x: x, y: y};
                        if (text === "cell") {
                            if (edit === 'texto' || edit === 'archivo') {
                                if (edit === 'texto')
                                    cell.innerHTML = "<input id='textEdit'>";
                                else if (edit === 'archivo') {
                                    cell.innerHTML = cell.innerHTML.substring(0, cell.innerHTML.lastIndexOf(">") + 1) + "<input id='textEdit'>";
                                }
                                var t = $("textEdit");

                                if (edit === 'texto') {
                                    t.value = cellData[y][x] ? cellData[y][x] : "";
                                    t.style.marginLeft = "0";
                                    if (cellData[y][x].trim().length === 0) {
                                        t.style.marginTop = "3px";
                                        t.style.marginBottom = "-3px";
                                    } else {
                                        t.style.marginTop = "0";
                                    }
                                } else if (edit === 'archivo') {
                                    t.value = cellData[y][x] ? cellData[y][x].name : "";
                                    t.style.marginLeft = "78px";
                                    t.style.marginTop = "-16px";
                                }
                                t.setSelectionRange(0, t.value.length);
                                t.style.display = "none";
                                t.style.display = "block";
                                t.focus();
                                t.onblur = function () {
                                    saveTextCell(t);
                                };
                                t.onkeyup = function (evt) {
                                    if (evt.ctrlKey) {
                                        if (evt.keyCode === 37) {
                                            saveTextCell(t, "left");
                                        } else if (evt.keyCode === 38) {
                                            saveTextCell(t, "up");
                                        } else if (evt.keyCode === 39) {
                                            saveTextCell(t, "right");
                                        } else if (evt.keyCode === 40) {
                                            saveTextCell(t, "down");
                                        }
                                    }
                                };
                                t.onkeypress = function (evt) {

                                    if (evt.keyCode === 13) {
                                        saveTextCell(t);
                                    }
                                };
                            } else {
                                alert("Falta Editar el tipo " + edit);
                            }
                        } else if (text === 'edit') {
                            var id = cellData[y][x].id;
                            htmlEdit(path, id, x, y, cellData[y][x].name);
                        }
                    }
                }

                var txtExt = ["", "txt", "md", "readme", "cfg", "properties", "json", "xml", "html", "htm", "jsp", "css", "js"];
                var imgExt = ["jpg", "jpeg", "gif", "png"];
                function htmlEdit(tabId, contId, x, y, name) {

                    var idx = name.lastIndexOf(".");
                    var ext = idx > -1 ? name.substring(idx + 1).toLowerCase() : "";
                    if (txtExt.indexOf(ext) > -1) {
                        loading(true);
                        showModal("editCode.jsp?tableId=" + encodeURI(tabId) + "&contentName=" + encodeURI(name) + "&contentId=" + encodeURI(contId) + "&x=" + x + "&y=" + y);
                    } else if (contId && imgExt.indexOf(ext) > -1) {
                        loading(true);
                        showModal("editImg.jsp?tableId=" + encodeURI(tabId) + "&contentName=" + encodeURI(name) + "&contentId=" + encodeURI(contId) + "&x=" + x + "&y=" + y);
                    } else {
                        err("Archivo no editable");
                    }
                    cellEdit = undefined;
                }

                function endEdit(text) {
                    var nx = nextX, ny = nextY;
                    var val = text.value;
                    cellEdit.cell.removeChild(text);
                    cellData[cellEdit.y][cellEdit.x] = val;
                    cellEdit.cell.innerHTML = val;
                    if (val.trim().length === 0)
                        val = "&nbsp;";
                    cellEdit = undefined;
                    doNext(nx, ny);
                }

                function endEditArchivo(name) {
                    var nx = nextX, ny = nextY;
                    var val = name.value;
                    if (cellEdit) {
                        cellEdit.cell.removeChild(name);
                        var obj = cellData[cellEdit.y][cellEdit.x];
                        obj.name = val;
                        if (val.trim().length === 0)
                            val = "&nbsp;";
                        cellEdit.cell.innerHTML = cellEdit.cell.innerHTML + val;
                        cellData[cellEdit.y][cellEdit.x] = obj;
                        cellEdit = undefined;
                    }
                    doNext(nx, ny);
                }

                function doNext(x, y) {
                    if (x !== null) {
                        var cell = $("c." + x + "." + y);
                        saveTextCellFlag = false;
                        edit(cell, def.columns[x].type, x, y, "cell");
                    }
                }

                var nextX, nextY;
                var saveTextCellFlag = false;
                function saveTextCell(text, next) {
                    if (!saveTextCellFlag) {
                        saveTextCellFlag = true;
                        if (next) {
                            nextX = cellEdit.x;
                            nextY = cellEdit.y;
                            if (next === "left" && nextX > 0) {
                                nextX--;
                            } else if (next === "up" && nextY > 0) {
                                nextY--;
                            } else if (next === "right" && nextX < def.columnas.length - 1) {
                                nextX++;
                            } else if (next === "down" && nextY < cellData.length - 1) {
                                nextY++;
                            }
                        } else {
                            nextX = null;
                            nextY = null;
                        }

                        var tip = def.columns[cellEdit.x].type;
                        if (tip === 'texto') {
                            if (cellData[cellEdit.y][cellEdit.x] !== text.value)
                                ajax("setData", [path, cellEdit.y, cellEdit.x, text.value], function () {
                                    endEdit(text);
                                    log("save");
                                    saveTextCellFlag = false;

                                });
                            else {
                                endEdit(text);
                                saveTextCellFlag = false;
                            }
                        } else if (tip === 'archivo') {
                            var name = cellData[cellEdit.y][cellEdit.x].name;
                            if (name !== text.value.trim()) {
                                ajax("setName", [path, cellEdit.y, cellEdit.x, text.value, name], function (resp) {
                                    cellData[cellEdit.y][cellEdit.x] = JSON.parse(resp);
                                    endEditArchivo(text);
                                    log("save");
                                    saveTextCellFlag = false;
                                });
                            } else {
                                endEditArchivo(text);
                                saveTextCellFlag = false;
                            }
                        }
                    }
                }

                function rerender(sel) {
                    var check = sel.checked;
                    sel.parentNode.parentNode.style.background = check ? "#226" : "#000";
                }

                function checkall(val) {
                    var che = _('check');
                    if (che) {
                        for (var i = 0; i < che.length; i++) {
                            che[i].checked = val;
                            rerender(che[i]);
                        }
                    }
                }

                function rowAdd() {
                    ajax("rowAdd", [path], function (resp) {
                        cellData.push(JSON.parse(resp));
                        renderData();
                    });
                }

                function rowDel() {
                    var che = _('check');
                    if (che) {
                        var list = [];
                        for (var i = 0; i < che.length; i++)
                            if (che[i].checked)
                                list.push(i);
                    }
                    if (list.length > 0)
                        ajax("rowDel", [path, list], function () {
                            for (var i = list.length - 1; i >= 0; i--)
                                cellData.splice(list[i], 1);
                            renderData();
                        });
                }

                addLogErrLoad();
                updateData(path);
                var el = document.getElementById('items');
                var fromIdx;
                var sortable = new Sortable(el, {
                    animation: 350,
                    handle: ".sort",
                    onSort: function (evt) {
                        ajax("sort", [path, fromIdx, evt.newIndex], function () {
                            var from = cellData[fromIdx];
                            cellData.splice(fromIdx, 1);
                            cellData.splice(evt.newIndex, 0, from);
                            renderData();
                        });
                    },
                    onStart: function (evt) {
                        fromIdx = Array.prototype.slice.call(evt.item.parentNode.children).indexOf(evt.item);
                    }
                });

        </script>
    </body>
</html>
