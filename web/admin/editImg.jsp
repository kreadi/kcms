<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String contentId = request.getParameter("contentId");
    String contentName = request.getParameter("contentName");
    String tableId = request.getParameter("tableId");
    int x = Integer.parseInt(request.getParameter("x"));
    int y = Integer.parseInt(request.getParameter("y"));
%>
<!DOCTYPE html>
<html manifest="editCode.appcache">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>KCMS Code Editor</title>
        <link rel='stylesheet' type='text/css' href='css/style.css'>
        <style>
            body{
                background: gray;
                margin:0;
                padding:0;
            }
            
        </style>
    </head>
    <body>
        &nbsp;<a href="javascript:save()" title="Guardar CTRL+S"><img src="icons/save.png" alt="save"></a>
        
        Editor de imagenes en desarrollo
        
        <script src="js/utils.js"></script>
        <script>
            parent.loading(false);
            function save() {
                ajax("setCode", ["<%=tableId%>", <%=y%>, <%=x%>, editor.getValue()], function (resp) {
                    parent.cellData[<%=y%>][<%=x%>] = resp;
                    parent.log("Código guardado...");
                });
            }

        </script>
    </body>
</html>
