<%@page import="java.lang.reflect.Method"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>AYUDA KCMS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel='stylesheet' type='text/css' href='css/tabla.css'>
    </head>
    <body>
        <h1>KREADI CMS</h1>

        <h2>Introducción</h2>
        <hr>
        <p>KCMS (Kreadi Content Management System) es un administrador de contenidos desarrollado en Java para Google App Engine que usa BeansShell 
            para simular los JSP.</p>
        <p>Los contenidos son almacenados en tablas y los tipos de datos soportados son: 
            String, Integer, Long, Float, Double, Boolean, StringSelect (Seleccionado entre opciones), Script (Codigo similar un JSP), 
            Html (Codigo Html), File (Archivos Binarios), Id (String unico) y SubTabla (Tabla dentro de Tabla)</p>

        <h2>JSP Simulado</h2>
        <hr>
        <p>
            cualquier archivo de tipo texto soporta scriptlets del tipo
            &lt;%CODIGO JAVA(BEANSHELL);%&gt; o &lt;%=VALOR%&gt;
        </p>
        <p>Dentro del codigo se pueden usar los siguientes metodos:</p>
                <ul>
            <li>log(Object s) //genera un registro de depuración en el log</li>
            <li>err(Object s) //genera un registro de error en el log</li>
            <li>process(String url) //Obtiene el codigo html o el resultado de un script procesado</li>
            <li>include(String url) //Inserta el codigo html o el resultado de un script procesado</li>
            <li>setData(String key, Object value) //Define un objeto que sera enviado a otro script</li>
            <li>getData(String key) //Obtiene un objeto de otro script</li>
            <li>append(Object s) //Inserta un texto</li>
            <li>include() //Indica la posicion donde se debe insertar el texto del script hijo en el script base</li>
            <li>base(String url) //Indica la url del script base</li>
            <li>getTabla(String url) //Obtiene una tabla</li>
        </ul>
        
        <h2>Metodos de la Clase Tabla</h2>
        <hr>
        <ul>
            <% Class cls = com.kreadi.server.model.DB.Tabla.class;
                Method[] metodos = cls.getMethods();
                for (Method m : metodos) {%>
            <li><%=m.getReturnType().getSimpleName() + " " + m.getName()%>( <%for (Class c : m.getParameterTypes()) {
                        out.print(c.getSimpleName() + " ");
                    }%>)</li>
                <%}%>
        </ul>
        <script src="js/utils.js"></script>
        <script>
            onEscape(parent.hideModal);
        </script>
    </body>
</html>
