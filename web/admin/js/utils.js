
var server = "/admin/serve";

/**
 * Remplaza el getElementById. ej: $("footer").innerHTML='xxx';
 * @param {type} id
 * @returns {Element}
 */
function $(id) {
    return document.getElementById(id);
}

/**
 * Remplaza el getElementByClassName. ej: _("boton").length --> cuantos elementos encontro con classname boton;
 * @param {type} className
 * @returns {Element}
 */
function _(className) {
    return document.getElementsByClassName(className);
}

/**
 * Ejecuta una funcion cuando un input type text pierde el foco o se presiona enter
 * @param {type} input elemento input a chequear
 * @param {type} func la funcion recibe como parametro el elemento input
 * @returns {undefined}
 */
function onInputChange(input, func) {
    input.onkeydown = function (e) {
        if (e.keyCode === 13) {
            func(e.target);
        }
    };
    input.onblur = function (e) {
        func(e.target);
    };
}

function onEscape(func) {
    document.onkeydown = function (e) {
        if (e.keyCode === 27)
            func.call(this);
    };
}

/**
 * Elimina espacios a la izquierda y derecha de un string. ej: "  Hola  ".trim() --> "Hola"
 * @returns {String.prototype@call;replace}
 */
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
};

/**
 * Retorna si un string es un id valido. ej: "123x xx".isId() --> false
 * @returns {Boolean}
 */
String.prototype.isId = function () {
    var validName = /^[$A-Z_][0-9A-Z_$]*$/i;
    return validName.test(this);
};

/**
 * Indica si un string comienza con una cadena especifica
 * @param {type} str
 * @returns {Boolean}
 */
String.prototype.startsWith = function (str) {
    return this.indexOf(str) === 0;
};

/**
 * Indica si un string termina con una cadena especifica
 * @param {type} str
 * @returns {Boolean}
 */
String.prototype.endsWith = function (str) {
    return this.indexOf(str) === this.length - str.length;
};

/**
 * Retorna el tipo de un objeto s:string, as:Array de String, n:number, b:boolean, _:null y undefined, o:objeto, f:function, x:otro
 * @param {type} obj
 * @returns {String}
 */
function getTipo(obj) {
    if (obj && typeof (obj) === "function")
        return "f";
    if (obj && obj.constructor === Array) {
        var t = getTipo(obj[0]);
        for (var i = 1; i < obj.length; i++) {
            var t0 = getTipo(obj[i]);
            if (t0 === t || t0 === null || t === null || t0 === undefined || t === undefined) {
                if (t0 !== null && t0 !== undefined) {
                    t = t0;
                }
            } else {
                t = 'o';
                break;
            }
        }
        t = (t === "_") ? "o" : t;
        return "a" + t;
    }
    var tip = typeof obj;
    tip = (tip === "string") ? "s" : (tip === "number") ? "n" : (tip === "boolean") ? "b" :
            (obj === "undefined" || obj === null) ? "_" : (tip === "object") ? "o" : "x";
    return tip;
}

/**
 * 
 * @param {type} metodo
 * @param {type} parametros
 * @param {type} onSuccess
 * @param {type} onError
 * @param {type} async
 * @returns {undefined}
 */
function ajax(metodo, parametros, onSuccess, onError, async) {
    if (getTipo(metodo) !== "s")
        throw new Exception("Debe indicar el nombre del metodo");
    if (parametros && getTipo(parametros)[0] !== "a") {
        throw new Exception("Los parametros deben estar en un array");
    }
    if (onSuccess && getTipo(onSuccess) !== "f") {
        throw new Exception("onSuccess debe ser una función");
    }
    if (onError && getTipo(onError) !== "f") {
        throw new Exception("onError debe ser una función");
    }
    var formData = new FormData();
    formData.append("m", metodo);
    if (parametros) {
        for (var i = 0; i < parametros.length; i++) {
            var tip = getTipo(parametros[i]);
            formData.append(i + tip, parametros[i].toString());
        }
    }
    var xhr = new XMLHttpRequest();
    async = async ? async : true;
    xhr.open("POST", server, async);
    xhr.onload = function () {
        if (xhr.status === 200) {
            var resp = xhr.responseText;
            if (resp.startsWith("{\"error\":\"")) {
                if (onError) {
                    var obj = JSON.parse(resp);
                    onError(obj.error, parametros);
                } else
                    alert(JSON.parse(resp).error);
            } else if (onSuccess)
                onSuccess(resp, parametros);
        } else {
            if (onError)
                onError(xhr.statusText, parametros);
            else
                err(xhr.statusText);
        }
    };
    xhr.send(formData);
}

//------------------------------------------------------------------------------------------------------------------

function fade(type, el, duration, IEsupport) {
    var isIn = (type === 'in'),
            IE = (IEsupport) ? IEsupport : false,
            opacity = isIn ? 0 : 1,
            interval = 50,
            gap = interval / duration;

    if (isIn) {
        el.style.display = 'block';
        el.style.opacity = opacity;
        if (IE) {
            el.style.filter = 'alpha(opacity=' + opacity + ')';
            el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity + ')';
        }
    }

    function func() {
        opacity = isIn ? opacity + gap : opacity - gap;
        el.style.opacity = opacity;
        if (IE) {
            el.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
            el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity * 100 + ')';
        }
        if (opacity <= 0 || opacity >= 1) {
            window.clearInterval(fading);
        }
        if (opacity <= 0) {
            el.style.display = "none";
        }
    }
    var fading = window.setInterval(func, interval);
}

function addLogErrLoad() {
    var body = document.body;
    body.innerHTML = body.innerHTML
            + "<div id='loading' style='position:fixed;background:#000 url(icons/loading.gif) center center no-repeat no-repeat;"
            + "width:100%;opacity:.7;z-index:666666666;top:0;bottom:0;display:none'></div>"
            + "<div id='modalBg' alt='cerrar'><div id='modalbox'><iframe id='iframe'></iframe></div></div>"
            + "<div style='z-index:666666667;display:none;color:white;background:red;position:fixed;bottom:4px;right:4px;padding:4px;opacity:0' id='err'></div>"
            + "<div style='z-index:666666668;display:none;color:white;background:green;position:fixed;bottom:4px;right:4px;padding:4px;opacity:0' id='log'></div>";
}

var isModal = false;
var modalBg, iframe;
function initModalVars() {
    if (!modalBg) {
        modalBg = document.getElementById("modalBg");
        iframe = document.getElementById("iframe");
        modalBg.onclick = function (evt) {
            if (evt.target.id === "modalBg")
                hideModal();
        };
    }
}

function showModal(url) {
    initModalVars();
    iframe.src = url;
    iframe.onload = function () {
        fade('in', $("modalBg"), 250, true);
        $("modalBg").style.display = "block";
        isModal = true;
    };
}
function hideModal() {
    initModalVars();
    if (iframe.src.indexOf('tabla.jsp') > -1) {
        updateData(path);
    }
    fade('out', $("modalBg"), 250, true);
    isModal = false;
}

function loading(load) {
    if (load)
        $("loading").style.display = "block";
    else
        $("loading").style.display = "none";
}

var _tout$;

function err(txt) {
    if (_tout$)
        clearTimeout(_tout$);
    fade('in', $("err"), 250, true);
    fade('out', $("log"), 250, true);
    _tout$ = setTimeout(function () {
        fade('out', $("err"), 250, true);
        setTimeout(function () {
            $("err").innerHTML = "";
            _tout$ = null;
        }, 350);
    }, txt.length * 200);
    $("err").innerHTML = txt;
    $("log").innerHTML = "";
}

function log(txt) {
    if (_tout$)
        clearTimeout(_tout$);
    fade('in', $("log"), 250, true);
    fade('out', $("err"), 250, true);
    _tout$ = setTimeout(function () {
        fade('out', $("log"), 250, true);
        setTimeout(function () {
            $("log").innerHTML = "";
            _tout$ = null;
        }, 350);
    }, txt.length * 200);
    $("log").innerHTML = txt;
    $("err").innerHTML = "";
}

history.pushState(null, null, location.href);
window.onpopstate = function () {
    history.go(1);
};
