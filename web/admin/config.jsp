<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>KCMS CONFIG</title>
        <link rel='stylesheet' type='text/css' href='css/style.css'>
        <style>
            body {
                color:black;
                margin:12px;
            }
            html{
                background: white;
            }
            button{
                font-size: 14px;
                border: none;
                padding: 4px;
                background: #ddf;
                border: 1px solid #999;
                box-sizing: border-box;
                outline: none;
                cursor: pointer;
            }
            input[type=text], select{
                font-size: 14px;
                width:100%;
                border: none;
                padding: 4px;
                background: #ffd;
                border: 1px solid #999;
                box-sizing: border-box;
                outline: none;
            }
        </style>
    </head>
    <body>
        PERFILES/USUARIOS
        <hr>
        <table style="width:100%" id="tabla"></table>
        <br>
        REDIRECCIONES
        <hr>
        <table style="width:100%">
            <tr>
                <td  style="width: 50%">
                    <input type="text" id="inicio" spellcheck='false' placeholder="PATH DE INICIO">
                </td>
                <td  style="width: 50%">
                    <input type="text" id="e404" spellcheck='false' placeholder="PATH ERROR 404">
                </td>
            </tr>
        </table>
        <br>
        REGLAS URL REGEXP
        <hr>
        <table style="width:100%" id="tabla2"></table>
        <script src="js/utils.js"></script>
        <script>
            addLogErrLoad();
            onEscape(parent.hideModal);
            var data = {data: []};
            function updateData() {
                var html = [];
                html.push("<tr><td style='min-width:80px'>Perfiles</td><td colspan=2 style='width:100%;'>Correos Gmail</td></td>");
                for (var i = 0; i < data.profiles.length; i++) {
                    html.push("<tr><td><input tabindex='-1' spellcheck='false' type='text' id='perfil" + i + "' value ='" + data.profiles[i][0] + "' readonly></td>");
                    html.push("<td style='width:100%;'><input spellcheck='false' class='correos' type='text' id='correos" + i + "' value='" + data.profiles[i][1] + "'></td>");
                    if (i > 0) {
                        html.push("<td style ='min-width:48px'><a  tabindex='-1' href='javascript: borrar(" + i + ")' title='Eliminar Perfil'>");
                        html.push("<img src='icons/del2.png'></a>");
                        html.push("<a  tabindex='-1' href='javascript: rename(\"" + data.profiles[i][0] + "\"," + i + ")' title='Renombrar Perfil'>");
                        html.push("<img src='icons/rename.png'></a></td>");
                    } else {
                        html.push("<td style ='min-width:48px'><a  tabindex='-1' href='javascript: nuevo()' title='Agregar Perfil'>");
                        html.push("<img src='icons/add2.png'></a></td>");
                    }
                    html.push("</tr>");
                }
                $("tabla").innerHTML = html.join("");
                $("inicio").value = data.init;
                $("e404").value = data.e404;


                html = [];
                html.push("<tr><td style='min-width:160px'>URL REGEXP</td><td style='min-width:140px;'>Procesar Siempre</td><td style='width:100%;'>Tipo Mime</td><td style='min-width:100px;'>Cache (mseg)</td>");
                html.push("<td><a tabindex='-1' href='javascript: newRule()' title='Agregar Regla'><img src='icons/add2.png'></a></td></tr>");
                var count = 0;
                for (var regexp in data.rules) {
                    var value = data.rules[regexp];
                    html.push("<tr><td><input tabindex='-1' spellcheck='false' type='text' id='rule" + i + "' value ='" + regexp + "' readonly></td>");
                    html.push("<td><input type='checkbox' id='proceso." + count + "' onchange='updateProceso(this)'" + (value.alwaysProcess?" checked":"") + "></td>");
                    html.push("<td><input type='text' id='mime." + count + "' value='" + value.mime + "'></td>");
                    html.push("<td><input type='number' id='expires." + count + "' value='" + value.expires + "'></td>");

                    html.push("<td style ='min-width:48px'><a  tabindex='-1' href='javascript: delRule(\"" + regexp + "\")' title='Eliminar Regla'>");
                    html.push("<img src='icons/del2.png'></a>");
                    html.push("<a  tabindex='-1' href='javascript: regexp(\"" + regexp + "\")' title='Modificar RegExp'>");
                    html.push("<img src='icons/rename.png'></a></td>");
                    html.push("</tr>");
                    count++;
                }

                $("tabla2").innerHTML = html.join("");
                count = 0;
                for (var regexp in data.rules) {
                    onInputChange($("mime." + count), updateMime);
                    onInputChange($("expires." + count), updateExpires);
                    count++;
                }

                var list = _("correos");
                for (var i = 0; i < list.length; i++) {
                    onInputChange(list[i], updateCorreos);
                }
                onInputChange($("inicio"), function (e) {
                    if (data.inicio !== e.value)
                        ajax("configRedireccion", ["inicio", e.value], function () {
                            data.init = e.value;
                            log("Redirección de inicio modificada...");
                        });
                });
                onInputChange($("e404"), function (e) {
                    if (data.e404 !== e.value)
                        ajax("configRedireccion", ["e404", e.value], function () {
                            data.e404 = e.value;
                            log("Redirección 404 modificada...");
                        });
                });
            }

            function getRegExp(idx) {
                var count = 0;
                for (var regexp in data.rules) {
                    if (count === idx)
                        return regexp;
                    count++;
                }
            }

            function updateProceso(e) {
                loading(true);
                var id = e.id;
                id = parseInt(id.substring(id.indexOf(".") + 1));
                id = getRegExp(id);
                var val = data.rules[id];
                ajax("configReglaSet", [id, e.checked, val.mime, parseInt(val.expires)], function (resp) {
                    data = JSON.parse(resp);
                    updateData();
                    loading(false);
                    log("Regla modificada...");
                });
            }

            function updateMime(e) {
               loading(true);
                var id = e.id;
                id = parseInt(id.substring(id.indexOf(".") + 1));
                id = getRegExp(id);
                var val = data.rules[id];
                ajax("configReglaSet", [id, val.alwaysProcess, e.value, parseInt(val.expires)], function (resp) {
                    data = JSON.parse(resp);
                    updateData();
                    loading(false);
                    log("Regla modificada...");
                });
            }

            function updateExpires(e) {
                loading(true);
                var id = e.id;
                id = parseInt(id.substring(id.indexOf(".") + 1));
                id = getRegExp(id);
                var val = data.rules[id];
                ajax("configReglaSet", [id, val.alwaysProcess, val.mime, parseInt(e.value)], function (resp) {
                    data = JSON.parse(resp);
                    updateData();
                    loading(false);
                    log("Regla modificada...");
                });
            }

            function updateCorreos(e) {
                var idx = parseInt(e.id.substring(7));
                var val = e.value;
                if (data.profiles[idx][1] !== val) {
                    ajax("configPerfilSet", [idx, val], function () {
                        data.profiles[idx][1] = val;
                        log("Correos Actualizados...");
                    });
                }
            }

            function update() {
                loading(true);
                ajax("configGet", null,
                        function (resp) {
                            data = JSON.parse(resp);
                            updateData();
                            loading(false);
                        },
                        function (resp) {
                            err(resp);
                            loading(false);
                        });
            }
            update();

            function newRule() {
                var regexp = prompt("Path RegExp");
                if (regexp) {
                    ajax("configReglaAdd", [regexp], function (resp) {
                        data = JSON.parse(resp);
                        updateData();
                        log("Nueva Regla agregada...");
                    });
                }
            }

            function nuevo() {
                var perfil = prompt("Perfil ID");
                if (perfil) {
                    perfil = perfil.trim().toLowerCase();
                    for (var i = 0; i < data.profiles.length; i++) {
                        if (data.profiles[i][0] === perfil) {
                            err("Identificador de perfil en uso");
                            return;
                        }
                    }
                    if (perfil.isId()) {
                        ajax("configPerfilAdd", [perfil], function (resp) {
                            data = JSON.parse(resp);
                            updateData();
                            log("Nuevo perfil agregado...");
                        });
                    } else {
                        err("Identificador de perfil incorrecto");
                    }
                }
            }

            function regexp(regExp) {
                newregexp = prompt("Path RegExp", regExp);
                if (newregexp && regExp !== newregexp) {
                    ajax("configReglaRen", [regExp, newregexp], function (resp) {
                        data = JSON.parse(resp);
                        updateData();
                        log("Path modificado...");
                    });
                }
            }


            function rename(perfil, idx) {
                perfil = prompt("Perfil ID", perfil);
                if (perfil) {
                    perfil = perfil.trim().toLowerCase();
                    for (var i = 0; i < data.profiles.length; i++) {
                        if (data.profiles[i][0] === perfil) {
                            err("Identificador de perfil en uso");
                            return;
                        }
                    }
                    if (perfil.isId()) {
                        ajax("configPerfilRen", [idx, perfil], function (resp) {
                            data = JSON.parse(resp);
                            updateData();
                            log("Perfil renombrado...");
                        });
                    } else {
                        err("Identificador de perfil incorrecto");
                    }
                }
            }

            function delRule(regexp) {
                ajax("configReglaDel", [regexp], function (resp) {
                    data = JSON.parse(resp);
                    updateData();
                    log("Regla eliminada...");
                });
            }

            function borrar(perfilIdx) {
                ajax("configPerfilDel", [perfilIdx], function (resp) {
                    data = JSON.parse(resp);
                    updateData();
                    log("Perfil eliminado...");
                });
            }
        </script>

    </body>
</html>