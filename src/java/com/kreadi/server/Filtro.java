package com.kreadi.server;

import bsh.EvalError;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.kreadi.server.model.DB;
import com.kreadi.server.model.Serial;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Filtro implements Filter {

    public final static HashSet<String> codeMap = new HashSet<>();

    static {
        codeMap.addAll(Arrays.asList(new String[]{"", "txt", "md", "readme", "cfg", "properties", "json", "xml", "html", "htm", "jsp", "css", "js"}));
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        setFilterConfig(fc);
    }

    FilterConfig config;

    public void setFilterConfig(FilterConfig config) {
        this.config = config;
    }

    public FilterConfig getFilterConfig() {
        return config;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader("X-FRAME-OPTIONS", "ALLOWALL");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String uri = req.getRequestURI();

        if (uri.equals("/admin") || uri.startsWith("/admin/") || uri.startsWith("/_ah/")) {
            chain.doFilter(request, response);
        } else {
            String filename;
            int idxDiv = uri.lastIndexOf("/");
            if (idxDiv == -1) {
                uri = "/" + uri;
                idxDiv = 0;
            }

            Serial ser0 = new Serial("config", new DB.Config()).load(true);
            DB.Config conf = (DB.Config) ser0.value;
            if (!ser0.loaded) {
                ser0.save(true);
            }
            if ("/".equals(uri)) {
                uri = conf.init;
            }

            String queryStr = req.getQueryString();
            String param = queryStr != null ? URLDecoder.decode(queryStr, "UTF-8") : null;
            filename = uri.substring(idxDiv + 1);

            int idxDot = filename.lastIndexOf(".");
            String ext = idxDot > -1 ? filename.substring(idxDot + 1).toLowerCase() : "";
            String mimeType = ext.length() == 0 ? "text/html" : null;
            if (mimeType == null) {
                ServletContext sc = getFilterConfig().getServletContext();
                mimeType = sc.getMimeType(filename);
            }
            resp.setContentType(mimeType);
            resp.setHeader("content-disposition", "inline; filename=\"" + filename + "\"");

            String expires = req.getParameter("expires");
            long now = System.currentTimeMillis();
            long expireTime = expires != null ? (Long.parseLong(expires)) : 0;
            resp.setDateHeader("Expires", now + expireTime);
            resp.setHeader("Cache-Control", "public, max-age=" + expireTime);
 
            boolean isCode = codeMap.contains(ext);
            if (isCode) {
                String respuesta;
                boolean process=DB.processRules(uri, resp);
                try {
                    respuesta = DB.process(uri, param, req, resp, null, null, process);
                } catch (Exception e) {
                    respuesta = e.getMessage();
                    resp.setContentType("text/plain");
                    resp.setStatus(500);
                }
                if (respuesta == null) {
                    resp.setContentType("text/html");
                    try {
                        respuesta = DB.process(conf.e404, null, req, resp, null, null, false);
                    } catch (Exception e) {
                        respuesta = e.getMessage();
                    }
                    resp.setStatus(404);
                }
                
                resp.setHeader("ETag", "" + respuesta.hashCode());
                resp.getWriter().print(respuesta);
            } else {
                String key = getFileKey(uri);
                if (key != null) {
                    BlobKey blobKey = new BlobKey(key);
                    BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
                    resp.setHeader("ETag", blobInfo.getMd5Hash());
                    resp.setHeader("Content-Disposition", "attachment; filename=" + filename);
                    DB.processRules(uri, resp);
                    blobstoreService.serve(blobKey, resp);
                }
            }
        }
    }

    private final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    private final BlobInfoFactory blobInfoFactory = new BlobInfoFactory();

    public <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    String getFileKey(String path) {
        Serial ser = new Serial("result" + path).getCache();
        if (ser.loaded) {
            return (String) ser.value;
        } else {
            int idx = path.lastIndexOf("/");

            String tabla = path.substring(0, idx > 0 ? idx : 1);
            String name = path.substring(idx + 1);
            ser = new Serial("tabla" + tabla).load(true);
            if (!ser.loaded) {
                return null;
            }
            DB.Tabla tab = (DB.Tabla) ser.value;
            int idxCol = -1;
            for (int i = 0; i < tab.columns.size(); i++) {
                if (tab.columns.get(i).type == DB.Column.Type.archivo) {
                    idxCol = i; 
                    break;
                }
            }
            if (idxCol == -1) {
                return null;
            }
            DB.FileInfo fi = null;
            for (Serializable[] data : tab.data) {
                DB.FileInfo fifi = (DB.FileInfo) data[idxCol];
                if (name.matches(fifi.name)) {
                    fi = fifi;
                    break;
                }
            }
            if (fi == null) {
                return null;
            }

            new Serial("result" + path, fi.id).setCache();
            return fi.id;

        }
    }

    @Override
    public void destroy() {
    }

}
