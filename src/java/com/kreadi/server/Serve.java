package com.kreadi.server;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.gson.Gson;
import com.kreadi.server.model.Manager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeMap;
import javax.servlet.ServletException;
import java.io.InputStream;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.kreadi.server.model.Serial;
import java.net.URLDecoder;

public class Serve extends HttpServlet {

    /**
     * Extiende el stream y le agrega la obtencion de los bytes en un rango
     */
    private final Class clsImpl = Manager.class;
    private Object objImpl;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            objImpl = clsImpl.getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new ServletException(ex);
        }
    }

    //s:string, as:Array de String, n:number, b:boolean, _:null y undefined, o:objeto, f:function, x:otro
    private String method(String method, TreeMap<String, String> params) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Class[] paramClass = new Class[params.size()];
        Object[] paramObj = new Object[params.size()];
        int i = 0;
        for (String key : params.keySet()) {
            Object o = params.get(key);
            key = key.replaceAll("\\d", "");
            Class c = null;
            if (null != key) {
                switch (key) {
                    case "s":
                        c = String.class;
                        break;
                    case "n":
                        c = Double.class;
                        o = new Double(o.toString());
                        break;
                    case "b":
                        c = Boolean.class;
                        o = "true".equals(o.toString());
                        break;
                    default:
                        if (key.startsWith("a")) {
                            Gson gson = new Gson();
                            switch (key.charAt(1)) {
                                case 's':
                                    c = String[].class;
                                    o = gson.fromJson("[" + o + "]", String[].class);
                                    break;
                                case 'n':
                                    c = Double[].class;
                                    o = gson.fromJson("[" + o + "]", Double[].class);
                                    break;
                                case 'b':
                                    c = Boolean[].class;
                                    o = gson.fromJson("[" + o + "]", Boolean[].class);
                                    break;
                                default:
                                    c = Object[].class;
                                    o = gson.fromJson("[" + o + "]", Object[].class);
                                    break;
                            }
                        } else {
                            c = Object.class;
                        }
                }
            }
            paramClass[i] = c;
            paramObj[i] = o;
            i++;
        }

        Method m = clsImpl.getMethod(method, paramClass);
        if (m == null) {
            return "{error:\"Metodo no encontrado\"}";
        }
        Object resp = m.invoke(objImpl, paramObj);

        String result = resp == null ? "" : resp.toString();
        return result;
    }

    private TreeMap<String, String> getParams(InputStream is, int buffSize) throws IOException {
        StringBuilder sb = new StringBuilder();
        int i;
        while ((i = is.read()) != 13) {
            sb.append((char) i);
        }
        String div = sb.toString();
        sb = new StringBuilder().append((char) 13);

        TreeMap<String, String> map = new TreeMap<>();
        byte[] bytes = new byte[buffSize];
        long read;
        int idx;
        while ((read = is.read(bytes)) > -1) {
            sb.append(new String(bytes, 0, (int) read, "UTF-8"));
            while ((idx = sb.indexOf(div)) > -1) {
                String txt = sb.substring(0, idx);
                int idx0 = txt.indexOf("\"") + 1;
                txt = txt.substring(idx0);
                idx0 = txt.indexOf("\"");
                String nam = txt.substring(0, idx0);
                txt = txt.substring(idx0 + 5, txt.length() - 2);
                map.put(nam, txt);
                sb.delete(0, idx + div.length());
            }
        }
        return map;
    }

    private final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    private final BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
    private final int cacheAge = 3600 * 24 * 14;//seconds = 1 dia

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String filename, key = URLDecoder.decode(req.getQueryString(),"UTF-8");
        boolean isCode = key.startsWith("file/");

        res.setContentType("application/octet-stream");
        int age = isCode ? 0 : cacheAge;
        long expiry = System.currentTimeMillis() + age * 1000;
        res.setDateHeader("Expires", expiry);
        res.setHeader("Cache-Control", "max-age=" + age);
        res.setHeader("Accept-Ranges", "bytes");

        if (isCode) {
            Serial json = new Serial(key).load(true);
            filename = key.substring(key.lastIndexOf("/") + 1);
            res.setHeader("ETag", "" + json.value.hashCode());
            res.setHeader("Content-Disposition", "attachment; filename=" + filename);
            res.getWriter().print(json.value.toString());
        } else {
            BlobKey blobKey = new BlobKey(key);
            BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
            filename = Manager.getFilename(key);
            res.setHeader("ETag", blobInfo.getMd5Hash());
            res.setHeader("Content-Disposition", "attachment; filename=" + filename);
            blobstoreService.serve(blobKey, res);
        }
        
    }

    Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        TreeMap map = getParams(req.getInputStream(), 2048);
        String method = (String) map.get("m");
        map.remove("m");
        try {
            String result = method(method, map);
            if (result != null) {
                resp.getWriter().write(result);
            }
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
            ex.printStackTrace();
            resp.getWriter().write("{\"error\":\"" + method + map.values() + " -> " + ex + "\"}");
        }
    }

}
