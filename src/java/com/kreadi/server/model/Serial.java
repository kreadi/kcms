package com.kreadi.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Serialize;
import java.io.Serializable;

/**
 * Asocia un objeto serializable asignado a un id(String)
 */
@Entity
public class Serial extends Entidad {

    private static final long serialVersionUID = -6214581437134474035L;
    @Id
    public String id;
    @Serialize
    public Serializable value;

    private Serial() {
    }

    public Serial(String key) {
        this.id = key;
    }
    
    public Serial(String key, Serializable serial) {
        this.id = key;
        this.value = serial;
    }

}
