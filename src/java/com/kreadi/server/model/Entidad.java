package com.kreadi.server.model;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.googlecode.objectify.Key;
import static com.kreadi.server.OfyService.ofy;
import java.io.Serializable;
import java.lang.reflect.Field;

public abstract class Entidad implements Serializable {

    private static final MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();

    public transient boolean loaded;

    public <T> T load() {
        return load(false);
    }

    public boolean exist() {
        try {
            Class<?> cls = getClass();
            Object id = cls.getField("id").get(this);
            if (id instanceof String) {
                return ofy().load().filterKey(Key.create(cls, (String) id)).keys().first().now() != null;
            } else {
                return ofy().load().filterKey(Key.create(cls, (Long) id)).keys().first().now() != null;
            }
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    public <T> T getCache() {
        try {
            Class cls = getClass();
            Object id = cls.getField("id").get(this);
            Object load = syncCache.get(id);
            
            if (load == null) {
                return (T) this;
            }
            
            for (Field f : cls.getFields()) {
                f.set(this, f.get(load));
            }
            
            this.loaded = true;
            return (T) this;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean setCache() {
        try {
            Class cls = getClass();
            Object id = cls.getField("id").get(this);
            syncCache.put(id, this);
            return true;
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    public <T> T load(boolean cache) {

        try {
            Class cls = getClass();
            Object id = cls.getField("id").get(this);
            Object load = null;
            if (cache) {
                load = syncCache.get(id);
            }
            if (load == null) {
                if (id instanceof String) {
                    load = ofy().load().key(Key.create(cls, (String) id)).now();
                } else if (id instanceof Long) {
                    load = ofy().load().key(Key.create(cls, (Long) id)).now();
                }
            }
            if (load == null) {
                return (T) this;
            }
            for (Field f : cls.getFields()) {
                f.set(this, f.get(load));
            }
            this.loaded = true;
            return (T) this;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean save() {

        return save(false);

    }

    public boolean save(boolean cache) {
        syncCache.clearAll();
        try {
            if (cache) {
                Class cls = getClass();
                Object id = cls.getField("id").get(this);
                syncCache.put(id, this);
            }
            ofy().save().entities(this).now();
            return true;
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean remove() {
        return remove(false);
    }

    public boolean remove(boolean cache) {
        try {
            if (cache) {
                Class cls = getClass();
                Object id = cls.getField("id").get(this);
                syncCache.delete(id);
            }
            ofy().delete().entities(this).now();
            return true;
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

}
