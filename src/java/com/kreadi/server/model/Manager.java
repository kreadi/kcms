package com.kreadi.server.model;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.gson.Gson;
import com.kreadi.server.model.DB.Config;
import com.kreadi.server.model.DB.Tabla;
import com.kreadi.server.model.DB.Column;
import com.kreadi.server.model.DB.FileInfo;
import com.kreadi.server.Filtro;
import com.kreadi.server.OfyService;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Manager {

    private static final Gson gson = new Gson();

    //--------------------- Configuración ------------------------------------------------------------
    public String configGet() {
        Serial json = new Serial("config").load(true);
        if (!json.loaded) {
            json = new Serial("config", new Config());
            json.save(true);
        }
        return gson.toJson(json.value);
    }

    public String configReglaAdd(String regexp) {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        if (!p.rules.containsKey(regexp)) {
            p.rules.put(regexp, new DB.PathRule());
        }
        json.save(true);
        return gson.toJson(json.value);
    }

    public String configReglaDel(String regexp) {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        if (p.rules.containsKey(regexp)) {
            p.rules.remove(regexp);
        }
        json.save(true);
        return gson.toJson(json.value);
    }

    public String configReglaSet(String regexp, Boolean proceso, String mime, Double expires) {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        if (p.rules.containsKey(regexp)) {
            p.rules.put(regexp, new DB.PathRule(proceso, mime, expires.longValue()));
        }
        json.save(true);
        return gson.toJson(json.value);
    }

    public String configPerfilAdd(String perfil) throws IOException, ClassNotFoundException, Exception {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        p.profiles.add(new String[]{perfil, ""});
        json.save(true);
        LinkedList<String> list = new LinkedList<>();
        for (String[] val : p.profiles) {
            list.add(val[0]);
        }
        new Serial("perfiles", list).save();
        return gson.toJson(json.value);
    }

    public String configPerfilDel(Double idx) {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        p.profiles.remove(idx.intValue());
        json.save(true);
        LinkedList<String> list = new LinkedList<>();
        for (String[] val : p.profiles) {
            list.add(val[0]);
        }
        new Serial("perfiles", list).save();
        return gson.toJson(json.value);
    }

    public String configReglaRen(String oldReg, String newReg) {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        TreeMap<String, DB.PathRule> map = new TreeMap<>();
        for (String key : p.rules.keySet()) {
            DB.PathRule ru = p.rules.get(key);
            if (key.equals(oldReg)) {
                key = newReg;
            }
            map.put(key, ru);
        }
        p.rules = map;
        json.save(true);
        return gson.toJson(json.value);
    }

    public String configPerfilRen(Double idx, String perfil) throws Exception {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        for (String[] row : p.profiles) {
            if (row[0].equals(perfil)) {
                throw new Exception("Ya existe el perfil " + perfil);
            }
        }
        String[] row = p.profiles.get(idx.intValue());
        String old = row[0];

        List<Serial> list2 = OfyService.ofy().load().type(Serial.class).filter("idx =", "tabla").list();
        for (Serial js : list2) {
            Tabla p2 = (Tabla) js.value;
            if (p2.profiles.contains(old)) {
                p2.profiles.remove(old);
                p2.profiles.add(perfil);
                js.value = p2;
                js.save();
            }
        }

        row[0] = perfil;
        p.profiles.set(idx.intValue(), row);
        json.save(true);
        LinkedList<String> list = new LinkedList<>();
        for (String[] val : p.profiles) {
            list.add(val[0]);
        }
        new Serial("perfiles", list).save();
        return gson.toJson(json.value);
    }

    public void configPerfilSet(Double idx, String correos) throws Exception {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        String[] row = p.profiles.get(idx.intValue());
        row[1] = correos;
        p.profiles.set(idx.intValue(), row);
        json.save(true);
    }

    public void configRedireccion(String param, String valor) throws Exception {
        Serial json = new Serial("config").load(true);
        Config p = (Config) json.value;
        if (null != param) {
            switch (param) {
                case "inicio":
                    p.init = valor;
                    break;
                case "e404":
                    p.e404 = valor;
                    break;
                default:
                    throw new Exception("Parametro de redirecciñon " + param + " no es valido");
            }
        } else {
            throw new Exception("Parametro de redirecciñon " + param + " no es valido");
        }
        json.save(true);
    }

    //--------------------- TABLAS -------------------------------------------------------
    public String tablasGet() {
        Serial json = new Serial("tablas").load();
        if (!json.loaded) {
            LinkedList<String> tab = new LinkedList<>();
            json.value = tab;
            json.save();
        }
        LinkedList<String> p = (LinkedList<String>) json.value;
        return gson.toJson(p);
    }

    public String tablasAdd(String path) throws IOException, ClassNotFoundException, Exception {
        path = path.trim();
        Serial json = new Serial("tablas").load();
        LinkedList<String> p = (LinkedList<String>) json.value;
        if (p.contains(path)) {
            throw new Exception("Ya existe esa tabla");
        }
        p.add(path);
        Collections.sort(p);
        json.save();
        String resp = gson.toJson(json.value);
        Tabla t = new Tabla();
        json = new Serial("tabla" + path, t);
        json.save();
        return resp;
    }

    public void tablaDel(String path) {
        path = path.trim();
        Serial json = new Serial("tablas").load();
        LinkedList<String> p = (LinkedList<String>) json.value;
        p.remove(path);
        json.save();
        json = new Serial("tabla" + path).load();
        json.remove();
        Tabla t = (Tabla) json.value;
        for (int i = 0; i < t.columns.size(); i++) {
            if (t.columns.get(i).type == Column.Type.archivo) {
                for (Object[] values : t.data) {
                    FileInfo fi = (FileInfo) values[i];
                    if (fi.id.startsWith("file/")) {
                        new Serial(fi.id).remove(true);
                    } else {
                        blobstoreService.delete(new BlobKey(fi.id));
                    }
                    deleteFilename(fi.id);
                }
            }
        }
    }

    public String tablasRen(String oldPath, String path) throws Exception {
        path = path.trim();
        oldPath = oldPath.trim();
        Serial json = new Serial("tablas").load();
        LinkedList<String> p = (LinkedList<String>) json.value;
        int idx = p.indexOf(oldPath);
        if (idx == -1) {
            throw new Exception("No se encontro la tabla con path " + oldPath);
        }
        p.set(idx, path);
        Collections.sort(p);
        json.save();
        String val = gson.toJson(json.value);

        json = new Serial("tabla" + oldPath).load();
        json.remove();
        json.id = "tabla" + path;
        json.save();
        return val;
    }

    //--------------------- TABLA -------------------------------------------------------
    public String getTable(String id) throws Exception {
        return gson.toJson(DB.getTable(id));
    }

    public String getPerfiles() {
        Serial json = new Serial("perfiles").load();
        if (!json.loaded) {
            json = new Serial("perfiles", new LinkedList<String>());
            json.save();
        }
        return gson.toJson(json.value);
    }

    public void setTablaDesc(String tabla, String desc) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.desc = desc;
        json.save();
    }

    public void setTablaPerfil(String tabla, String perfil, Boolean check) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        if (check) {
            p.profiles.add(perfil);
        } else {
            p.profiles.remove(perfil);
        }
        json.save();
    }

    public void setTablaEditable(String tabla, Boolean edit) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.edit = edit;
        json.save();
    }

    //--------------------- COLUMNAS -------------------------------------------------------
    public String colAdd(String tabla) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla t = (Tabla) json.value;
        t.columns.add(new Column());

        int count = 0;
        for (Object[] s : t.data) {
            Serializable[] news = new Serializable[s.length + 1];
            System.arraycopy(s, 0, news, 0, s.length);
            news[s.length] = "";
            t.data.set(count, news);
            count++;
        }
        json.save();
        String val = gson.toJson(json.value);
        return val;
    }

    public void colEdit(String tabla, Double idx, Boolean check) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.columns.get(idx.intValue()).edit = check;
        json.save();
    }

    public void colName(String tabla, Double idx, String nombre) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.columns.get(idx.intValue()).title = nombre;
        json.save();
    }

    public void colTipo(String tabla, Double idx, String tipo) {
        Column.Type tip = null;
        for (Column.Type t : Column.Type.values()) {
            if (tipo.equals(t.toString())) {
                tip = t;
                break;
            }
        }
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.columns.get(idx.intValue()).type = tip;
        json.save();
    }

    public void colAncho(String tabla, Double idx, String ancho) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.columns.get(idx.intValue()).width = Integer.parseInt(ancho);
        json.save();
    }

    public void colReglas(String tabla, Double idx, String reglas) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.columns.get(idx.intValue()).rules = reglas;
        json.save();
    }

    public void colSort(String tabla, Double from, Double to) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla t = (Tabla) json.value;
        Column c = t.columns.get(from.intValue());
        t.columns.remove(from.intValue());
        t.columns.add(to.intValue(), c);

        for (int i = 0; i < t.data.size(); i++) {
            Serializable[] s = t.data.get(i);
            Serializable s0 = s[from.intValue()];
            List<Serializable> os = new LinkedList<>(Arrays.asList(s));
            os.remove(from.intValue());
            os.add(to.intValue(), s0);
            os.toArray(s);
            t.data.set(i, s);
        }
        json.save();
    }

    public String colDel(String tabla, Double idx) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla t = (Tabla) json.value;
        int col = idx.intValue();
        boolean isArchivo = t.columns.get(col).type == Column.Type.archivo;
        t.columns.remove(col);
        int count = 0;

        for (Object[] s : t.data) {
            if (isArchivo) {
                String id = ((FileInfo) s[col]).id;
                if (id != null) {
                    if (id.startsWith("file/")) {
                        new Serial(id).remove(true);
                    } else {
                        blobstoreService.delete(new BlobKey(id));
                    }
                    deleteFilename(id);
                }
            }
            Serializable[] news = new Serializable[s.length - 1];
            System.arraycopy(s, 0, news, 0, col);
            System.arraycopy(s, col + 1, news, col, s.length - col - 1);
            t.data.set(count, news);
            count++;
        }
        json.save();
        return gson.toJson(json.value);
    }

    //--------------------- TABLAS -------------------------------------------------------
    public String getData(String tabla) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla t = (Tabla) json.value;
        return gson.toJson(t.data);
    }

    public String rowAdd(String tabla) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla t = (Tabla) json.value;
        String val;
        Serializable[] obj = new Serializable[t.columns.size()];
        for (int i = 0; i < t.columns.size(); i++) {
            Column.Type tipo = t.columns.get(i).type;
            if (tipo == Column.Type.texto) {
                obj[i] = "";
            } else if (tipo == Column.Type.archivo) {
                obj[i] = new FileInfo();
            } else {
                obj[i] = "";
            }
        }
        t.data.add(obj);
        val = gson.toJson(obj);
        json.save();
        return val;
    }

    public void rowDel(String tabla, Double[] idx) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        List<Integer> archivoColIdx = new LinkedList<>();
        for (int i = 0; i < p.columns.size(); i++) {
            Column c = p.columns.get(i);
            if (c.type == Column.Type.archivo) {
                archivoColIdx.add(i);
            }
        }
        for (int i = idx.length - 1; i >= 0; i--) {
            for (int c : archivoColIdx) {
                FileInfo fi = (FileInfo) p.data.get(idx[i].intValue())[c];
                if (fi.id != null) {
                    if (fi.id.startsWith("file/")) {
                        new Serial(fi.id).remove(true);
                    } else {
                        blobstoreService.delete(new BlobKey(fi.id));

                    }
                    deleteFilename(fi.id);
                }
            }
            p.data.remove(idx[i].intValue());
        }
        json.save();
    }

    public void setData(String tabla, Double row, Double col, String obj) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        p.data.get(row.intValue())[col.intValue()] = obj;
        json.save();
    }

    public void sort(String tabla, Double rowFrom, Double rowTo) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        Serializable[] from = p.data.get(rowFrom.intValue());
        p.data.remove(rowFrom.intValue());
        p.data.add(rowTo.intValue(), from);
        json.save();
    }

    private static final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    public String uploadUrl() {
        return blobstoreService.createUploadUrl("/admin/upload");
    }

    public static void setFilename(String id, String filename) {
        Serial val = new Serial("filenames").load(true);
        HashMap<String, String> map = (HashMap<String, String>) val.value;
        if (map == null) {
            map = new HashMap<>();
        }

        String oldName = map.get(id);

        String lastKey = null;
        if (oldName != null) {
            for (Entry<String, String> entry : map.entrySet()) {
                if (oldName.equals(entry.getValue())) {
                    lastKey = entry.getKey();
                    break;
                }
            }
        }
        if (lastKey != null) {
            map.remove(lastKey);
        }

        map.put(id, filename);
        val.value = map;
        val.save(true);
    }

    public static String getFilename(String id) {
        if (id.startsWith("file/")) {
            return id.substring(id.lastIndexOf("/") + 1);
        }
        Serial val = new Serial("filenames").load(true);
        if (!val.loaded) {
            return null;
        }
        HashMap<String, String> map = (HashMap<String, String>) val.value;
        return map.get(id);
    }

    public static void deleteFilename(String id) {
        Serial val = new Serial("filenames").load(true);
        if (!val.loaded) {
            val.value = new HashMap<>();
        }
        HashMap<String, String> map = (HashMap<String, String>) val.value;
        map.remove(id);
        val.save(true);
    }

    public String setArchivo(String tabla, String name, String id, Double col, Double row, Double size) {

        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        FileInfo fi = (FileInfo) p.data.get(row.intValue())[col.intValue()];//Obtiene el id antiguo

        int idx = name.lastIndexOf(".");
        String ext = idx > -1 ? name.substring(idx + 1).trim().toLowerCase() : "";
        boolean isCode = Filtro.codeMap.contains(ext);

        if (fi.id != null) {
            if (fi.id.startsWith("file/")) {
                new Serial(fi.id).remove(true);
            } else {
                blobstoreService.delete(new BlobKey(fi.id));
            }
            deleteFilename(fi.id);
        }
        fi.id = id;
        fi.size = size.intValue();
        fi.name = name;

        if (isCode) {
            byte[] bytes = blobstoreService.fetchData(new BlobKey(fi.id), 0, fi.size);
            String to = new String(bytes);
            String fileId;
            Serial ser;
            do {
                fileId = "file/" + System.currentTimeMillis();
                ser = new Serial(fileId, to);
            } while (ser.exist());
            ser.save();
            blobstoreService.delete(new BlobKey(fileId));
            fi.id = fileId;
        }

        p.data.get(row.intValue())[col.intValue()] = fi;
        setFilename(id, name);
        json.save();
        return id;
    }

    public String setName(String tabla, Double row, Double col, String name, String oldName) {
        Serial json = new Serial("tabla" + tabla).load();
        Tabla p = (Tabla) json.value;
        FileInfo fi = (FileInfo) p.data.get(row.intValue())[col.intValue()];
        fi.name = name;
        p.data.get(row.intValue())[col.intValue()] = fi;
        if (fi.id != null) {
            setFilename(fi.id, name);
        }
        json.save();
        return gson.toJson(fi);
    }

    public String setCode(String tabla, Double row, Double col, String code) {
        try {
            Serial json = new Serial("tabla" + tabla).load();
            Tabla d = (Tabla) json.value;
            Serializable[] obj = (Serializable[]) d.data.get(row.intValue());
            FileInfo fi = (FileInfo) d.data.get(row.intValue())[col.intValue()];

            if (fi.id != null) {
                if (fi.id.startsWith("file/")) {
                    new Serial(fi.id).remove(true);
                } else {
                    blobstoreService.delete(new BlobKey(fi.id));
                }
                deleteFilename(fi.id);
            }

            String fileId;
            Serial ser = null;
            do {
                fileId = "file/" + System.currentTimeMillis();
                ser = new Serial(fileId, code);
                fi.id = fileId;
            } while (ser.exist());
            setFilename(fileId, tabla + fi.name);
            ser.save(true);

            fi.size = code.length();
            obj[col.intValue()] = fi;
            d.data.set(row.intValue(), obj);
            json.save();
            return (String) gson.toJson(obj[col.intValue()]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
