/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kreadi.server.model;

import bsh.EvalError;
import bsh.Interpreter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DB {

    public static class Tabla implements Serializable {

        private static final long serialVersionUID = 374984379843794387L;
        public String desc = "";
        public transient String id;
        public boolean edit = true;
        public List<String> profiles = new LinkedList<>();
        public List<Column> columns = new LinkedList<>();
        public List<Serializable[]> data = new LinkedList<>();

        public Tabla() {
            Column col = new Column();
            col.type = Column.Type.archivo;
            columns.add(col);
        }

        public int getRows() {
            return data.size();
        }

        public int getCols() {
            return columns.size();
        }

        public Column getCol(int idx) {
            return columns.get(idx);
        }

        public String getString(int col, int row) {
            return (String) data.get(row)[col];
        }

        public boolean getBoolean(int col, int row) {
            return (Boolean) data.get(row)[col];
        }

        public int getInt(int col, int row) {
            return (Integer) data.get(row)[col];
        }

        public long getLong(int col, int row) {
            return (Long) data.get(row)[col];
        }

        public Date getDate(int col, int row) {
            return (Date) data.get(row)[col];
        }

        public String getFileName(int col, int row) {
            return id + "/" + ((FileInfo) data.get(row)[col]).name;
        }

        public int getFileSize(int col, int row) {
            return ((FileInfo) data.get(row)[col]).size;
        }

        public String getFileId(int col, int row) {
            return ((FileInfo) data.get(row)[col]).id;
        }

        public String getCode(int col, int row) throws Exception {
            return DB.getCode(id, col, row);
        }

    }

    public static class Column implements Serializable {

        private static final long serialVersionUID = 848746376487364386L;

        public enum Type {

            texto, numero, fecha, logico, opcion, html, archivo, id, subtabla
        };

        public String title;
        public boolean edit;
        public int width;
        public Type type;
        public String rules;

        public Column() {
            title = "";
            edit = true;
            width = 0;
            type = Type.texto;
            rules = "";
        }
    }

    public static class FileInfo implements Serializable {

        private static final long serialVersionUID = -8946387648643874638L;
        public String id = null, name = "";
        public int size = 0;
    }

    public static class PathRule implements Serializable {

        private static final long serialVersionUID = -5476895798579345642L;
        public boolean alwaysProcess = false;
        public String mime = "";
        public long expires = 0;

        public PathRule(boolean alwaysProcess, String mime, long expires) {
            this.alwaysProcess = alwaysProcess;
            this.mime = mime;
            this.expires = expires;
        }

        public PathRule() {
        }

    }

    public static class Config implements Serializable {

        private static final long serialVersionUID = -5738945789345793465L;

        public List<String[]> profiles = new LinkedList<>();
        public String init = "/";
        public String e404 = "/404";
        public TreeMap<String, PathRule> rules = new TreeMap<>();

        public Config() {
            profiles.add(new String[]{"admin", ""});
        }
    }

    public static Tabla getTable(String id) throws Exception {
        Serial json = new Serial("tabla" + id).load();
        if (json.loaded) {
            Tabla t = (Tabla) json.value;
            t.id = id;
            return t;
        } else {
            throw new Exception("Tabla " + id + " no existe");
        }
    }

    public static String getCode(String path) throws Exception {

        int idx = path.lastIndexOf("/");

        String tabla = path.substring(0, idx > 0 ? idx : 1);
        String name = path.substring(idx + 1);
        DB.Tabla tab = getTable(tabla);
        int idxCol = -1;
        for (int i = 0; i < tab.columns.size(); i++) {
            if (tab.columns.get(i).type == DB.Column.Type.archivo) {
                idxCol = i;
                break;
            }
        }
        if (idxCol == -1) {
            throw new Exception("Columna tipo archivo no existe en tabla " + tabla);
        }
        DB.FileInfo fi = null;
        for (Serializable[] data : tab.data) {
            DB.FileInfo fifi = (DB.FileInfo) data[idxCol];
            if (name.matches(fifi.name)) {
                fi = fifi;
                break;
            }
        }
        if (fi == null) {
            throw new Exception("Archivo " + name + " no existe en Tabla " + tabla + "");
        }
        Serial ser = new Serial(fi.id).load(true);
        if (ser.loaded) {
            return (String) ser.value;
        } else {
            throw new Exception("Archivo " + name + " no existe en Tabla " + tabla + " (id:" + fi.id + ")");
        }
    }

    public static String getCode(String table, int col, int row) throws Exception {

        DB.Tabla tab = getTable(table);
        int idxCol = -1;
        for (int i = 0; i < tab.columns.size(); i++) {
            if (tab.columns.get(i).type == DB.Column.Type.archivo) {
                idxCol = i;
                break;
            }
        }
        if (idxCol == -1) {
            throw new Exception("Columna tipo archivo no existe en tabla " + table);
        }
        String fid = tab.getFileId(col, row);
        Serial ser = new Serial(fid).load(true);
        if (ser.loaded) {
            return (String) ser.value;
        } else {
            throw new Exception("Archivo (" + row + ", " + col + ") no existe en Tabla " + table + " (id:" + fid + ")");
        }
    }

    public static boolean processRules(String uri, HttpServletResponse response) {
        Serial ser = new Serial("config", new DB.Config()).load(true);
        if (!ser.loaded) {
            ser.save(true);
        }
        Config conf = (Config) ser.value;
        boolean process = false;
        String ur = uri.toLowerCase();
        for (String key : conf.rules.keySet()) {
            if (ur.matches(key)) {
                PathRule rule = conf.rules.get(key);
                process = rule.alwaysProcess;
                if (rule.mime.trim().length() > 0) {
                    response.setContentType(rule.mime);
                }
                if (rule.expires > 0) {
                    long now = System.currentTimeMillis();
                    long expireTime = rule.expires;
                    response.setDateHeader("Expires", now + expireTime);
                    response.setHeader("Cache-Control", "public, max-age=" + expireTime);
                }
                break;
            }
        }
        return process;
    }

    private static Pattern haveScript = Pattern.compile("<%.*%>");

    /**
     * Procesa el script en beanshell
     *
     * @param uri
     * @param urlParam
     * @param request HTTP requerimiento
     * @param response HTTP Respuesta
     * @param include texto a incluir cuando llame al metodo include() dentro del script
     * @param data objeto que se comparte con el parent
     * @param process
     * @return retorna el string del script procesado
     *
     * @throws EvalError
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static String process(String uri, String urlParam, HttpServletRequest request, HttpServletResponse response, String include, HashMap<String, Object> data, boolean process) throws Exception {

        Serial cacheSerial = null;
        if (!process) {
            cacheSerial = new Serial("result" + uri + (urlParam != null ? "?" + urlParam : "")).getCache();
        }
        if (!process && cacheSerial.loaded) {
            return (String) cacheSerial.value;
        } else {
            String script = getCode(uri);
            if (!haveScript.matcher(script).find()) {
                if (!process) {
                    cacheSerial.value = script;
                    cacheSerial.setCache();
                }
                return script;
            } else {
                StringBuilder sb;
                String content = null;
                try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); PrintStream pst = new PrintStream(baos); ByteArrayOutputStream baos2 = new ByteArrayOutputStream(); PrintStream pst2 = new PrintStream(baos2)) {
                    int idx = uri.lastIndexOf("/");
                    String tabla = idx != -1 ? uri.substring(0, idx + 1) : "/";
                    String code = getJavaCode(script);
                    StringReader reader = new StringReader(code);
                    Interpreter bsh = new Interpreter(reader, pst, pst2, false);
                    sb = new StringBuilder();
                    bsh.set("sb", sb);
                    bsh.set("path", tabla);
                    bsh.set("include", include);
                    bsh.set("data", data);
                    bsh.set("request", request);
                    bsh.set("response", response);
                    bsh.run();
                    Exception e = (Exception) bsh.get("exception");
                    if (e != null) {
                        e.printStackTrace(pst2);
                    }
                    byte[] berr = baos2.toByteArray();
                    if (berr.length > 0) {
                        if (berr.length > 0) {
                            sb.append("\nERROR --------------------------\n").append(new String(berr)).append("\n");
                        }
                        if (e == null) {
                            StringBuilder sb2 = new StringBuilder("1: " + code);
                            int count = 1;
                            for (int i = 0; i < sb2.length(); i++) {
                                if (sb2.charAt(i) == '\n') {
                                    count++;
                                    sb2.insert(i + 1, count + ": ");
                                }
                            }
                            sb.append(sb2);
                            sb.append("\n");
                        }
                        response.setContentType("text/pain");
                        response.setStatus(500);
                    } else {
                        content = (String) bsh.get("content");
                        data = (HashMap) bsh.get("data");
                    }
                }
                if (content != null) {//Si indico la posicion del include
                    String result = process(content, urlParam, request, response, sb.toString(), data, process);
                    cacheSerial.value = result;
                    cacheSerial.setCache();
                    return result;
                } else {
                    String result = sb.toString();
                    if (!process) {
                        cacheSerial.value = result;
                        cacheSerial.setCache();
                    }
                    return result;
                }
            }
        }
    }

    public static String processPath(String actualPath, String newPath) throws Exception {
        try {
            newPath = newPath.startsWith("/") ? newPath : actualPath + newPath;
            newPath = (newPath + "/").replaceAll("\\/\\.\\/", "/");
            int idx, idx0;
            while ((idx = newPath.indexOf("/../")) > -1) {
                idx0 = newPath.substring(0, idx).lastIndexOf("/");
                newPath = newPath.substring(0, idx0) + newPath.substring(idx + 3);
            }
            return newPath.substring(0, newPath.length() - 1);
        } catch (Exception e) {
            throw new Exception("Path relativo incorrecto...");
        }

    }

    private static String getJavaCode(String scriptletCode) {
        String preCode = "import com.kreadi.server.model.DB;\n"
                + "content=null;\n"
                + "exception=null;\n"
                + "log(Object s){\n"//log
                + "     System.out.println(s);\n"
                + "};\n"
                + "err(Object s){\n"//err
                + "     System.err.println(s);\n"
                + "};\n"
                + "process(String url){\n"//Obtiene el texto un html o de un script procesado
                + "     url=DB.processPath(path,url);\n"
                + "     String queryStr = request.getQueryString();\n"
                + "     String param = queryStr != null ? URLDecoder.decode(queryStr, \"UTF-8\") : null;\n"
                + "     return DB.process(url, param, request, response, null, data, true);\n"
                + "}\n"
                + "include(String url){\n"//Incluye el texto un html o de un script procesado
                + "     append(process(url));\n"
                + "}\n"
                + "setData(String key, Object value){\n"//Incluye un texto
                + "     if (data==null) data=new HashMap();\n"
                + "     data.put(key, value);\n"
                + "};\n"
                + "getData(String key){\n"//Incluye un texto
                + "     if (data==null) return null;\n"
                + "     return data.get(key);\n"
                + "};\n"
                + "append(Object s){\n"//Incluye un texto
                + "     sb.append(s);\n"
                + "     return sb;\n"
                + "};\n"
                + "include(){\n"//Incluye el texto de otro script que es indicado con el metodo content
                + "     append(include);\n"
                + "};\n"
                + "base(String url){\n"//Indica la url del script contenedor
                + "     url=DB.processPath(path,url);\n"
                + "     content=url;\n"
                + "};\n"
                + "getTable(String url){\n"//Obtiene una tabla
                + "     url=DB.processPath(path,url);\n"
                + "     return DB.getTable(url);\n"
                + "};\n"
                + "try{\n";
        StringBuilder sb = new StringBuilder();
        sb.append(preCode);
        LinkedList<String> sections = new LinkedList<>();
        int lastPos = 0, state = 0;
        char c0 = '\0', c1;
        for (int pos = 0; pos < scriptletCode.length(); pos++) {
            c1 = c0;
            c0 = scriptletCode.charAt(pos);
            if (state == 0 && c0 == '%' && c1 == '<') {
                sections.add(scriptletCode.substring(lastPos, pos - 1));
                lastPos = pos - 1;
                state = 1;
            } else if (state == 1 && c1 == '%' && c0 == '>') {
                sections.add(scriptletCode.substring(lastPos, pos + 1));
                lastPos = pos + 1;
                state = 0;
            }
        }
        int size = scriptletCode.length();
        if (lastPos != size) {
            sections.add(scriptletCode.substring(lastPos, size));
        }

        for (String s : sections) {
            size = s.length();
            if (size > 0) {
                if (s.startsWith("<%") && s.endsWith("%>")) {
                    if (s.startsWith("<%=")) {
                        sb.append("\tsb.append(");
                        sb.append(s.substring(3, size - 2).trim());
                        sb.append(");\n");
                    } else {
                        sb.append(s.substring(2, size - 2)).append("\n");
                    }
                } else {
                    s = s.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\"").
                            replace("\r", "\\r").replace("\t", "\\t");
                    sb.append("\tsb.append(\"");
                    sb.append(s);
                    sb.append("\");\n");
                }
            }
        }
        sb.append("} catch(Exception e){\n\texception=e;\n}");
        String result = sb.toString();
        return result;
    }

}
